/**
	* @file logger.h
	* @author Nikola Milev
	* @date 13.09.2016.
 	* @brief Header file containing various variable and function declarations.
 	*
 	*	This project serves as logging system with both text and binary logging allowed.
 	*	It was made as a part of my project on an internship but it is pretty general.
 	*	For any additional information, contact me at the email address provided below.
 	*
 	* @email nikola.n.milev@gmail.com
 	* @see http://www.stack.nl/~dimitri/doxygen/docblocks.html
 	* @see http://www.stack.nl/~dimitri/doxygen/commands.html
	* @note Errno is not altered in any of the functions and can be used after they fail to determine the reason of the failure
 */

#ifndef LOGGER_A_
#define LOGGER_A_

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>

/** Maximum path length allowed in the progam. */
#define PATH_LEN_ 20

/** Delimiter character for the start of timestamp. */
#define TIMEDELIM_START_ '['
/** Delimiter character for the end of timestamp. */
#define TIMEDELIM_END_ ']'
/** Delimiter character for separating days, months and years in the date inside the timestamp. */
#define TIMEDELIM_DATE_ '-'
/** Delimiter character for separating hours, minutes and second inside the time inside the timestamp. */
#define TIMEDELIM_TIME_ ':'
/** Delimiter character for separating date and time inside the timestamp. */
#define TIMEDELIM_DATE_TIME_ ' '
/** Delimiter character for the beginning of the logged message. */
#define MSGDELIM_START_ '{'
/** Delimiter character for the end of the logged message. */
#define MSGDELIM_END_ '}'

/** If returned by a function or set as the error argument, indicates that everything is okay and the execution went without any issues. */
#define OK_ 0
/** If returned by a function or set as the error argument, indicates that there has been an allocation error. */
#define ALLOC_FAIL_ -1
/** If returned by a function or set as the error argument, indicates that there has been an error opening a file. */
#define OPEN_FAIL_ -2
/** If returned by a function or set as the error argument, indicates that there has been an error reading a file. */
#define READ_FAIL_ -3
/** If returned by a function or set as the error argument, indicates that the argument that is supposed to contain the logging parameter NULL . */
#define BAD_LOG_PATH_ -4
/** If returned by a function or set as the error argument, indicates that there has been an error writing to a file. */
#define WRITE_FAIL_ -5

/** Defines the length of the timestamp to be printed. */
#define TIMESTAMP_LEN_ 24

/**
	* This is needed because UNIX like systems return the number of seconds passed since January 1st, 1900. The month ranges
	* from 0 to 11, the day ranges from 1 to 31 and the year is the current year subtracted by 1900.
	*/
#define ORIGIN_YEAR_ 1900

//Main problem remains how to make sure the file is not written to or damaged after recieving a signal from the OS

/** Calls log_msg_(), enabling the user to not have to manually type path to the log file but instead use the default. */
#define LOG_MSG_(MSG, TIMESTAMP_) log_msg_(MSG, TIMESTAMP_, LOGPATH_)
/** Calls log_msg_(), enabling the user to not have to manually type path to the log file and timestamp but instead use the default path and current time. */
#define LOG_MSG_NOW_(MSG) log_msg_now_(MSG, LOGPATH_)
/** Calls log_msg_(), enabling the user to not have to manually type path to the log file but instead use the default path. Uses the LOGERRPATH_ for logging errors*/
#define LOG_ERROR_(ERR, TIMESTAMP_) log_msg_(ERR, TIMESTAMP_, LOGERRPATH_)
/** Calls log_msg_(), enabling the user to not have to manually type path to the log file and timestamp but instead use the default path and current time.
	* Uses the LOGERRPATH_ for logging errors
	*/
#define LOG_ERROR_NOW_(ERR) log_msg_now_(ERR, LOGERRPATH_)
/** Calls log_bin_(), enabling the user to not have to manually type path to the binary log file but instead use the default. */
#define LOG_BINARY_(MSG,LEN) log_bin_(MSG, LEN, LOGBINPATH_)
/** Calls read_bin_full_data_(), enabling the user to not have to manually type path to the binary log file but instead use the default. */
#define READ_BINARY_(ERR_CODE) read_bin_full_data_(LOGBINPATH_, ERR_CODE) ;
/** Calls read_bin_parsed_(), enabling the user to not have to manually type path to the binary log file but instead use the default. */
#define READ_BINARY_PARSED_(ERR_CODE, TIME_, SIZE_) read_bin_parsed_(LOGBINPATH_, ERR_CODE, TIME_, SIZE_) ;
/** Calls truncate_by_(), enabling the user to not have to manually type path to the binary log file but instead use the default. */
#define TRUNC_BIN_(AMOUNT) truncate_by_(LOGBINPATH_, AMOUNT)
/** Calls truncate_last_bin_(), enabling the user to not have to manually type path to the binary log file but instead use the default. */
#define TRUNC_LAST_BIN_() truncate_last_bin_(LOGBINPATH_)

/**
	* UNIX systems (including Linux distributions and Mac OS) use '\n' as newline character and Windows uses '\n\r'
	* string (char*) is used here on purpose because you cannot write '\n\r' into one char variable since that's two characters and not one
	*/
char NEWLINE_STRING_[3] ;
/** Indicates if a newline string is to be used while doing non-binary logging. */
char SEPARATE_BY_NEWLINE_ ;

/**
	* Size of header (the header will contain a time_t variable, showing the time of the logging and a size_t variable, showing the size of the
	* logged data, in that order)
	*/
#define HEADER_LEN_ (sizeof(time_t) + sizeof(size_t))

char LOGPATH_[PATH_LEN_] ;			/*!< The defalt log path that will be used with the macros above */
char LOGERRPATH_[PATH_LEN_] ;		/*!< The defalt error log path that will be used with the macros above */
char LOGBINPATH_[PATH_LEN_] ;		/*!< The defalt binary log path that will be used with the macros above */
FILE *logFile ;						/*!< Universal FILE* variable used for all the logging attempts */

//
size_t LAST_READ_SIZE_BIN_ ; /*!< The size of the last read message, with the header. It is reset to 0 every time we call truncate_by_() */

/** These two unions allow me to read sizeof(time_t) (or sizeof(size_t) in the second union) bytes and interpret them as time_t / size_t
	* The inability to cast directly is called strict aliasing.
	* Since I cannot treat time_t data as char[sizeof(time_t)], I used the union.
	* Unions use as much space as the largest field uses, so, in this case, the same.
	* @see http://stackoverflow.com/questions/98650/what-is-the-strict-aliasing-rule
	*/
typedef union t{
	time_t time_f;
	char char_f[sizeof(time_t)];
}time_str_;

/** See the explanation for the above structure */
typedef union s{
	time_t size_f;
	char char_f[sizeof(size_t)];
}size_str_;

/**
	* @brief A wrapper around fopen() call.
	*
	* This function calls fopen() and also  retries if the call was interrupted by a signal, until successful or
	* failed for a reason other than signal interrupt.
	* @param path The path of the file to be opened
	* @param mode The mode in which to open the file.
	* @return FILE* pointer to the newly open file.
	* @see fopen man page
 */
FILE* open_log_(const char *path, const char *mode) ;

/**
	* @brief Prints a two digit number val to the dest buffer.
	*
	* Prints a two - digit number to the dest ; If the number has only 1 digit, 0 is appended to the front.
	* For numbers > 99 (more than 2 digits), the lowest two digits are observed.
	* @param dest Destination buffer, the location of the string containing the two-digit value converted to string
	* @param val The value to be converted to the string
	* @note This function's purpose lies solely in creating the timestamp.
	* @return None
	*/
void print_small_(char *dest, int val) ;

/**
	* @brief Returns a formatted timeprint containing information about current time.
	*
	* Prints the timestamp in a following format: [HH:MM:SS DD-MM-YYY TMZN].
	* Here, TMZN (timezone string) is not of fixed length and everything else is. Also, the
	* characters [, ], :, - and the blank character can be changed before compilation by altering the #define directives above
	* @param None
	* @return The string containing the timestamp representing the current time or, if failed, NULL.
	*/
char* get_time_(void) ;

/**
	* @brief Prints a message msg to the file stream on path logpath.
	*
	* Prints a message msg to the file stream on path logpath. Opens and closes the path itself.
	* Also appends a the timestamp provided as an argument.
	obtained using the function get_time_().
	* @param msg The message that is to be logged
	* @param logpath The path of the file the message is to be logged to.
	* @param timestamp: String representing the timestamp.
	* @return An indicator if everything is okay. The possible values are provided and explained above.
	*/
int log_msg_(const char* msg, const char* timestamp, const char* logpath) ;

/**
	* @brief The wrapper around the log_msg_() function.
	*
	* Also appends a the timestamp obtained using the function get_time_().
	* @param msg The message that is to be logged
	* @param logpath The path of the file the message is to be logged to.
	* @return An indicator if everything is okay. The possible values are provided and explained above.
	* @see log_msg_() function
	*/
int log_msg_now_(const char* msg, const char* logpath) ;

/**
	* @brief Append data buffer to the file with the path logpath, along with a header.
	* Append data buffer to the file with the path logpath. A header is written before the data, uniquely detemrining
	* the time of the writing and the size of data
	* The first time_t bytes of the record written contain the time of the creation of the record
	* and the size_t bytes after that contain the length of the rest of the record
	* Therefore, the record written is time_t + size_t + sizeof(unsigned char)*length bytes long
	* @param data the sequence of bytes to be read
	* @param length the length of the abovementioned sequence
	* @param logpath the path to the log file
	* @return An indicator if everything is okay. The possible values are provided and explained above.
	* @note sizeof(unsigned char) is 1 (as well as sizeof(char)) but this just seemed a more elegant way to write it.
	*/
int log_bin_(const char *data, const size_t length, const char* logpath) ;

/**
	* @brief Remove the last record from the binary file and return its contents.
	*
	* The return buffer also contains the header, which is of fixed size after the compilation.
	* The first time_t bytes of the logpath buffer contain the time of the creation of the record
	* and the size_t bytes after that contain the length of the rest of the buffer
	* the return buffer is allocated inside the function and, when possible, always fits the size of the header + data perfectly
	* The file is @b NOT truncated to remove the unlogged message, as it was in the previous versions.
	* @param logpath The path to the log file
	* @param err An indicator if everything is okay. The possible return values are defined above.
	* @return The read sequence, starting with the header, if everything is successful; NULL otherwise.
	* @note The return buffer is not parsed so the user will have to do it on his/her own. If the user wants a parsed read, see read_bin_parsed_()
	* @note Since I can not guarantee that sizeof(time_t) and sizeof(size_t) are the same on all machines, I cannot guarantee that data logged on one machine can be properly read on another one
	*/
char* read_bin_full_data_(const char* logpath, int *err) ;

/**
	* @brief A wrapper around unlog_bin_full_data_() that parses the output
	*
	* This is a wrapper for unlog_bin_full_data_(), which receives the path, the address of the variable to write error to, the address
	* of the time_t variable to write the time part of the header and the size_t variable to write the size part of the header to,
	* meaning that the user is not obliged to do the parsing of the data if he/she doesn't want to
	* @param logpath The path to the log file
	* @param err An indicator if everything is okay. See the possible return values above.
	* @param t The read time structure that indicates the time of the logging of the read data.
	* @param s The read size_t variable that holds the size of the read data, in bytes.
	* @return The buffer, with the header removed, or NULL, if failed.
	*/
char* read_bin_parsed_(const char* logpath, int *err, time_t *t, size_t *s) ;

/**
	* @brief Truncate file by the specified path by amount bytes.
	*
	* Since the call truncate() truncates to a certain length, this is more convenient if you want to truncate by a certain amount
	* @param path The path of the file we are truncating
	* @param amount The amount of bytes that are to be removed from the end of file
	* @return An indicator if everything is okay. See the possible return values above.
	* @note The WRITE_FAIL_ constant in the return value is used to indicate truncate() call failure.
	*/
int truncate_by_(const char *path, unsigned amount) ;

/**
	* @brief Wrap around the truncate_by_() call to remove the last read message
	*
	* Wrap around the truncate_by_() call using LAST_READ_SIZE_ as the amount argument.
	* Also resets LAST_READ_SIZE_ to 0 if (and only if) the truncate_by_() call was successful
	* @param path The path of the file we are truncating
	* @return An indicator if everything is okay. See the possible return values above.
	* @note The WRITE_FAIL_ constant in the return value is used to indicate truncate() call failure.
	* @see truncate_by_()
	*/
int truncate_last_(const char *path) ;
#endif

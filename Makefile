PROGRAM = logger
MAIN = main
LOGGER = logger

CC_FLAGS =  -Wall

$(PROGRAM): $(MAIN).c $(LOGGER).c 
	gcc $^ $(CC_FLAGS) -o $@

PHONY: .clean

clean:
	rm -f *.o *.h.gch $(LOGGER) 

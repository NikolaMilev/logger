#include <time.h>
#include "logger.h"

int main(int argc, char **argv)
{
	char *data ;
	time_t t;
	size_t s;
	int err=11;
	int i;
	char c[18] = "ABCDEFABCDEFABCDEF";
	c[0] = 255;
	log_bin_(c, 18, "logbin.txt");

	printf("log: %d\n", LOG_MSG_NOW_("majkata"));
	printf("log: %d\n", LOG_ERROR_NOW_("TATKOTI"));


	data = READ_BINARY_PARSED_(&err, &t, &s);

	printf("ERROR: %d\n", err);

	if(err < 0)
	{
		printf("greskica :(\n");
		return 1;
	}
	else if(data == NULL)
	{
		printf("nista\n");
		return 1;
	}

	printf("\n");
	printf("TIME: %lu\nDATA: ", t);
	for(i=0; i<s; i++)
	{
		//Also, hhu can be used for writing unsigned char
		printf("%u ", (unsigned char)data[i]);
	}

	TRUNC_BIN_(s + HEADER_LEN_);
	return 1;
}
